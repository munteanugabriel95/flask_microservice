from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(app)

class videosTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    views = db.Column(db.Integer, nullable=False)
    likes = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"Video(name = {self.name}, views = {self.view}, likes = {self.likes})"


db.create_all()
videos_put_args = reqparse.RequestParser()
videos_put_args.add_argument("name", type=str, help="Name of the video is missing", required=True)
videos_put_args.add_argument("views", type=int, help="views of the video is missing", required=True)
videos_put_args.add_argument("likes", type=int, help="likes on the video is missing", required=True)

video_update_args = reqparse.RequestParser()
video_update_args.add_argument("name", type=str, help="Name of the video is missing")
video_update_args.add_argument("views", type=int, help="views of the video is missing")
video_update_args.add_argument("likes", type=int, help="likes on the video is missing")


resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'views': fields.Integer,
    'likes': fields.Integer,
}



class Video(Resource):
    @marshal_with(resource_fields)
    def get(self, video_id):
        result = videosTable.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message='Could not find any video with that Id')
        return result, 200

    @marshal_with(resource_fields)
    def put(self, video_id):
        args = videos_put_args.parse_args()
        result = videosTable.query.filter_by(id=video_id).first()
        if result:
            abort(409, message="Video id taken...")

        video = videosTable(id=video_id, name=args['name'], views=args['views'], likes=args['likes'])
        db.session.add(video)
        db.session.commit()
        return video, 201

    @marshal_with(resource_fields)
    def patch(self, video_id):
        args = video_update_args.parse_args()
        result = videosTable.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message='Could not find any video with that Id')

        if args['name']:
            result.name = args['name']
        if args['views']:
            result.views = args['views']
        if args['likes']:
            result.likes = args['likes']

        db.session.add(result)
        db.session.commit()

        return result, 200

    def delete(self, video_id):
        result = videosTable.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message='Could not find any video with that Id')

        # result.delete()
        db.session.delete(result)
        db.session.commit()

        return {"Status": f"Video id {video_id} removed succesfully"}, 204


class massVideosOps(Resource):
    @marshal_with(resource_fields)
    def get(self):
        result = videosTable.query.all()
        return result

api.add_resource(Video, "/video/<int:video_id>")
api.add_resource(massVideosOps, "/massVideoOps")


if __name__ == "__main__":
    app.run(debug=True)
