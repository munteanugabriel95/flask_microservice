import requests
import unittest
import pytest

#Test Positive Scenarios
class Test_API(unittest.TestCase):
    BaseURL = "http://127.0.0.1:5000/"

    @pytest.mark.order(1)
    def test_put_method(self):
        #Clear table
        allVideos = requests.get(self.BaseURL + f"massVideoOps").json()

        for i in range(len(allVideos)+1):
            requests.delete(self.BaseURL + f"video/{i}")

        data = [{"name": "Spartacus", "views": 999123, "likes": 12120},
                {"name": "Gabi", "views": 24525, "likes": 113230},
                {"name": "Munt", "views": 5435, "likes": 321321}]

        for count in range(len(data)):
            rsp = requests.put(self.BaseURL + f"video/{count}", data[count])
            self.assertEqual(rsp.status_code, 201)
            self.assertGreater(len(rsp.json()), 0)

    @pytest.mark.order(2)
    def test_get_method(self):
        # input("get Video id..")
        rsp = requests.get(self.BaseURL + "video/2")
        self.assertEqual(rsp.status_code, 200)
        self.assertGreater(len(rsp.json()), 0)
        return rsp.json()

    @pytest.mark.order(3)
    def test_patch_method(self):
        # input("update Video id..")
        rsp = requests.patch(self.BaseURL + "video/2", {"views": 2030210321})
        self.assertEqual(rsp.status_code, 200)
        self.assertGreater(len(rsp.json()), 0)

    @pytest.mark.order(4)
    def test_delete_method(self, vid_id=0):
        # input("delete Video id..")
        rsp = requests.delete(self.BaseURL + f"video/{vid_id}")
        self.assertEqual(rsp.status_code, 204)


if __name__ == "__main__":
    tester = Test_API()
    tester.test_put_method()
